﻿-- CONSULTAS DE COMBINACIÓN INTERNAS

-- Nombre y edad de los ciclistas que han ganado etapas.

  SELECT DISTINCT c.nombre, c.edad FROM etapa e
  JOIN ciclista c
  ON c.dorsal = e.dorsal
  GROUP BY c.nombre, c.edad;

-- Nombre y edad de los ciclistas que han ganado puertos.

  SELECT DISTINCT c.nombre, c.edad FROM puerto p
  JOIN ciclista c
  ON c.dorsal = p.dorsal
  GROUP BY c.nombre, c.edad;

-- Nombre y edad de los ciclistas que han ganado etapas y puertos.

  SELECT c.nombre, c.edad FROM etapa e
  JOIN puerto p
  ON e.dorsal = p.dorsal
  JOIN ciclista c
  ON e.dorsal = c.dorsal
  GROUP BY c.nombre, c.edad;

  -- Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa.

  SELECT DISTINCT e.director FROM equipo e
  JOIN (ciclista c JOIN etapa e1
  ON c.dorsal = e1.dorsal)
  ON e.nomequipo = c.nomequipo;
 
  -- Dorsal y nombre de los ciclistas que hayan llevado algún maillot.

  SELECT DISTINCT l.dorsal, c.nombre FROM lleva l
  JOIN ciclista c
  ON l.dorsal = c.dorsal
  GROUP BY c.nombre;

  -- Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo.
  
  SELECT l.dorsal, c.nombre FROM lleva l
  JOIN ciclista c
  ON l.dorsal = c.dorsal
  GROUP BY l.dorsal;

  -- Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas.

  SELECT DISTINCT e.dorsal FROM lleva l
  JOIN etapa e
  ON e.dorsal = l.dorsal;
  
  -- Indicar el numetapa de las etapas que tengan puertos.

  SELECT DISTINCT  p.numetapa FROM puerto p;

  -- Indicar los kms de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos.

  SELECT DISTINCT etapa.kms, etapa.numetapa FROM (ciclista c
  JOIN etapa 
  ON c.dorsal=etapa.dorsal)
  JOIN puerto ON etapa.numetapa = puerto.numetapa
  WHERE (((c.nomequipo)='banesto'));

  -- Listar el nº de ciclistas que hayan ganado alguna etapa con puerto.

  SELECT DISTINCT e.dorsal FROM etapa e
  JOIN puerto p
  ON e.numetapa = p.nompuerto;

  -- Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto.

  SELECT p.nompuerto FROM ciclista c
  JOIN puerto p
  ON c.dorsal = p.dorsal
  WHERE (((c.nomequipo)= 'banesto'));

  -- Listar el nº de etapas que tengan puerto que hayan sido ganadas por ciclistas de Banesto con más de 200 kms.
  
  SELECT DISTINCT e.numetapa FROM (ciclista c
  JOIN etapa e
  ON c.dorsal = e.dorsal)
  JOIN puerto p
  ON e.numetapa = p.nompuerto
  WHERE (((c.nomequipo)='banesto') AND ((e.kms)>=200));



  